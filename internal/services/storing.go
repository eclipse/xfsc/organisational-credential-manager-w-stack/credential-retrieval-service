package services

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwe"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/jwt"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	logPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/credential"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/types"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-retrieval-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
)

var log logPkg.Logger

func EncryptResponse(response credential.CredentialResponse, pub jwk.Key) *jwe.Message {
	byteArray, err := json.Marshal(response.Credential)
	if err != nil {
		log.Error(err, "Failed to serialized Response")
	}
	message := jwt.EncryptJweMessage(byteArray, jwa.ECDH_ES_A256KW, pub)
	return message
}

func StoreCredential(tenantId, requestId, groupId string, response credential.CredentialResponse, pub jwk.Key, ctx context.Context) error {
	log = ctxPkg.GetLogger(ctx)
	var cred = response.Credential
	var contentType = ""
	if pub != nil {
		cred = EncryptResponse(response, pub)
		contentType = "application/jose"
	}

	var byteCredential = make([]byte, 0)
	if response.Format == string(types.JWTVC) || response.Format == string(types.SDJWT) {
		byteCredential = []byte(cred.(string))
	} else {
		c, err := json.Marshal(cred)

		if err != nil {
			log.Error(err, "error in marshalling message")
			return err
		}

		byteCredential = c
	}

	storemessage := messaging.StorageServiceStoreMessage{
		Request: common.Request{
			TenantId:  tenantId,
			RequestId: requestId,
			GroupId:   groupId,
		},
		Id:          response.CNonce,
		AccountId:   groupId,
		Type:        "credential",
		Payload:     byteCredential,
		ContentType: contentType,
	}

	b, err := json.Marshal(storemessage)

	if err != nil {
		log.Error(err, "error in marshalling message")
	}

	client, err := cloudeventprovider.New(cloudeventprovider.Config{
		Protocol: cloudeventprovider.ProtocolTypeNats,
		Settings: cloudeventprovider.NatsConfig{
			Url:          config.CurrentCredentialRetrievalConfig.Nats.Url,
			QueueGroup:   config.CurrentCredentialRetrievalConfig.Nats.QueueGroup,
			TimeoutInSec: config.CurrentCredentialRetrievalConfig.Nats.TimeoutInSec,
		},
	}, cloudeventprovider.ConnectionTypePub, config.CurrentCredentialRetrievalConfig.StoringTopic)

	if err != nil {
		log.Error(err, err.Error())
	}

	event, err := cloudeventprovider.NewEvent("retrieval-service", messaging.StoreCredentialType, b)
	if err != nil {
		log.Error(err, err.Error())
	}

	if err = client.PubCtx(ctx, event); err != nil {
		log.Error(err, fmt.Sprintf("sending event failed: %s", err))
	}
	return err
}
